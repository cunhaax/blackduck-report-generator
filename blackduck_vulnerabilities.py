#!/usr/bin/env python3

import json
from blackduck.HubRestApi import HubInstance
from jinja2 import Environment, FileSystemLoader
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import re
import settings


class BlackduckVulnerabilities:

    def __init__(self, report_action):
        self.hub = HubInstance(settings.BLACKDUCK_URL, api_token=settings.API_TOKEN, insecure=True)
        self.report_action = report_action

    def get_project(self):
        return [i for i in self.hub.get_projects()['items']
                if i['name'] == settings.PROJECT_NAME][0]

    def get_version(self, project):
        versions_result = self.hub.get_project_versions(project)
        return [i for i in versions_result['items']
                if i['versionName'] == settings.VERSION_NAME][0]

    def get_all_vulnerable_components(self, version):
        links = {e['rel']: e['href'] for e in version['_meta']['links']}

        # get all vulnerable-components (limit=0)
        v_comps = self.hub.execute_get(
                links['vulnerable-components'] + self.hub.get_limit_paramstring(0)
                )
        return json.loads(v_comps.content)

    def filter_vulnerable_components(self, components, severity='HIGH'):
        # filter by severity
        return [i for i in components['items']
                if i['vulnerabilityWithRemediation']['severity'] == severity]

    def get_resume(self, components):
        res = []
        for i in components:
            matched_files_link = [e['href'] for e in i['_meta']['links']
                                  if e['rel'] == 'matched-files']
            r = {'ver': i['componentVersionOriginId'],
                 'vulnerability': i['vulnerabilityWithRemediation']}

            matched_files = self.hub.execute_get(
                                matched_files_link[0] + self.hub.get_limit_paramstring(0)
                                )
            json_matched_files = json.loads(matched_files.content)
            matched_archives = set([e['filePath']['archiveContext']
                                    for e in json_matched_files['items']])
            r['components'] = []
            for f in matched_archives:
                match = re.search('^(.+\.zip).*', f)
                if match:
                    key = match.group(1)
                    r['components'].append('{} : {}'.format(
                        settings.COMPONENTS_MAP.get(key, ''), f)
                        )

            res.append(r)
        return res

    def run(self):
        project = self.get_project()

        version = self.get_version(project)
        stats = {e['countType']: e['count']
                 for e in version['securityRiskProfile']['counts']}

        all_vulnerable_components = self.get_all_vulnerable_components(version)
        high_severity = self.filter_vulnerable_components(all_vulnerable_components, 'HIGH')

        report = []
        report += self.get_resume(high_severity)

        medium_severity = self.filter_vulnerable_components(all_vulnerable_components, 'MEDIUM')

        report += self.get_resume(medium_severity)

        self.report_action.run(stats, report)


class EmailReport:

    def __init__(self):
        pass

    def generate_html(self, stats, report):
        env = Environment(loader=FileSystemLoader('templates'))
        template = env.get_template('email-template.html')
        html_report = template.render({'stats': stats,
                                       'reports': report,
                                       'project_name': settings.PROJECT_NAME,
                                       'version_name': settings.VERSION_NAME})
        # TODO delete this, only for debug
        # with open('index.html', 'w') as f:
        #     f.write(html_report)
        return html_report

    def send_email(self, content):
        sender = settings.EMAIL_SENDER
        receivers = settings.EMAIL_RECEIVERS
        msg = MIMEMultipart('alternative')
        msg['Subject'] = 'Vulnerabilities report | {} {}'.format(
                settings.PROJECT_NAME, settings.VERSION_NAME
                )
        msg['From'] = sender
        msg['To'] = ','.join(receivers)
        part1 = MIMEText(content, 'html')
        msg.attach(part1)

        with smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT) as server:
            server.sendmail(sender, receivers, msg.as_string())

    def run(self, stats, report):
        html_report = self.generate_html(stats, report)

        self.send_email(html_report)


def main():

    BlackduckVulnerabilities(EmailReport()).run()
    # print(json.dumps(stats, indent=2))
    # print(json.dumps(report, indent=2))


if __name__ == '__main__':
    main()
