# Features #

Email report generator that connects to a Black Duck Hub to build a vulnerability report
showing essential info (which components of a project have the vulnerabilities).

Report example:
![Report example](doc/example.png)

# API Documentation #
https://github.com/blackducksoftware/hub-rest-api-python

# How to install dependencies #

Install dependencies with a virtual environment

```sh
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements-py3.txt
```

# How to run #

1. Create a file with the name **settings.py** from the settings template and edit it according to your needs
2. Run:

```sh
source .venv/bin/activate
./blackduck_vulnerabilities.py
```
